#!/bin/sh

BASEDIR=$(dirname "$(realpath $0)")

sudo apt install i3 -y

ln -sf $BASEDIR/config $HOME/.config/i3/config
