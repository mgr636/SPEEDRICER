#!/bin/sh

BASEDIR=$(dirname "$(realpath $0)")

curl -LO https://github.com/neovim/neovim/releases/download/v0.7.2/nvim-linux64.deb
sudo mv ./nvim-linux64.deb /var/cache/apt/archives/nvim-linux64.deb
sudo apt install /var/cache/apt/archives/nvim-linux64.deb
mkdir $HOME/.config/nvim

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# install NerdFonts
curl -LO "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip"
mkdir $HOME/.fonts
unzip $BASEDIR/SourceCodePro.zip -d $HOME/.fonts/
rm $HOME/.config/nvim/SourceCodePro.zip
fc-cache -fv

# required by VimPlug
sudo apt install exuberant-ctags nodejs npm -y
sudo npm install n yarn -g
sudo n lts #recommended
#sudo n latest #latest
sudo apt purge nodejs npm -y

# create symlink
ln -sf $BASEDIR/init.vim $HOME/.config/nvim/init.vim
ln -sf /home/mgr636/.local/share/nvim/plugged /home/mgr636/.config/nvim/plugged

# need pip to install language servers
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt install python3.10 -y
sudo apt install python3-pip -y
pip install pynvim --upgrade
