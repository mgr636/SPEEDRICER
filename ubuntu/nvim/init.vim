" mgr636's init.vim

" Appearance
set number
set relativenumber
set cursorline
set showmatch
syntax enable


" Tab/Indent
set smarttab
set expandtab
set smartindent
set autoindent
set shiftwidth=4
set tabstop=4


" Searching
set incsearch


" Others
set fenc=utf-8
set noswapfile
set autoread


" Plugins
call plug#begin()
Plug 'https://github.com/ap/vim-css-color'
Plug 'https://github.com/preservim/nerdtree'
Plug 'https://github.com/preservim/tagbar'
Plug 'https://github.com/rafi/awesome-vim-colorschemes'
Plug 'https://github.com/ryanoasis/vim-devicons'
Plug 'https://github.com/tc50cal/vim-terminal'
"Plug 'https://github.com/tpope/vim-surround'
Plug 'https://github.com/vim-airline/vim-airline'

Plug 'https://github.com/neoclide/coc.nvim'
call plug#end()


" NERDTree
let g:NERDTreeDirArrowExpandable="+"
let g:NERDTreeDirArrowCollapsible="~"


" Keymapping
inoremap jk <ESC>
inoremap <silent><expr> <Tab>
    \ coc#pum#visible() ? coc#pum#next(1) :
    \ CheckBackspace() ? "\<Tab>" :
    \ coc#refresh()
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <Space>w :w<CR>
nnoremap <Space>q :q<CR>
nnoremap <Space>e :NERDTreeToggle<CR>
nnoremap <Space>/ :TagbarToggle<CR>


" Colorscheme
colorscheme jellybeans

" Functions
function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~ '\s'
endfunction
