#!/usr/bin/perl

use strict;
use warnings;
use Cwd 'getcwd';

my $BASE_DIR = getcwd;
my %APP_LIST;
my %INSTALLED;

sub show_list {

    sub get_dots {
        my $msg = shift @_;

        my $msg_length = length($msg);
        my $dots_count = 60 - $msg_length;
        return '.' x $dots_count;
    }

    my $dir_path = shift @_;
    my @app_list = fetch_applist($dir_path);
    my $index = 1;
    for my $app_name (@app_list) {

        $INSTALLED{$index} = 0;
        my $msg = " $index: $app_name";

        if ($INSTALLED{$index} == 1) {
            my $dots = get_dots($msg);
            print "$msg $dots (installed)\n";
        } else {
            print $msg, "\n";
        }

        $APP_LIST{$index} = $app_name;
        $index++;
    }
    print " a: Install above all.\n";
    print " q: Quit installation.\n";
}

sub fetch_applist {
    my $base_dir = shift @_;
    my @app_list;
    opendir my $dh, $base_dir or die "Could not open directory $base_dir: $!";
    for my $dirname (readdir($dh)) {
        # only need dirs
        next if ($dirname =~ /\./);
        push(@app_list, $dirname);
    }
    return @app_list;
}

sub show_welcome {
    system("clear");
    print " ==========================================================\n";
    print "  ____  ____  _____ _____ ____  ____  ___ ____ _____ ____  \n";
    print " / ___||  _ \\| ____| ____|  _ \\|  _ \\|_ _/ ___| ____|  _ \\ \n";
    print " \\___ \\| |_) |  _| |  _| | | | | |_) || | |   |  _| | |_) |\n";
    print "  ___) |  __/| |___| |___| |_| |  _ < | | |___| |___|  _ < \n";
    print " |____/|_|   |_____|_____|____/|_| \\_\\___\\____|_____|_| \\_\\\n";
    print "\n";
    print " ============= Welcome to mgr636's installer ==============\n";
    print "\n";
}

sub exec_command {
    my $command = shift @_;

    if ($command eq "a") {
        for my $key(keys(%APP_LIST)) {
            print "$APP_LIST{$key}\n";
        }
    }

    if ($command eq "q") {
        $command = undef;
    }

    print $BASE_DIR/$0, "\n";
}

sub main {

    &show_welcome();

    my $command;
    until (defined($command)) {

        &show_list($BASE_DIR);
        print "\n COMMAND >> ";
        my $command = <STDIN>;
        chomp $command;

        &exec_command($command);
    }
}

&main()
